========================================================================
         libfreebsd: a library of routines obtained from FreeBSD
========================================================================

:Version: 1.14.1833
:SPDX-License-Identifier: BSD-2-Clause
:Copyright:
   The portions of this codebase that exist only to support its for
   portability and redistribution are
   Copyright 2021-22, The University of New South Wales, Australia.

   Much of this distribution falls under FreeBSD's copyright conditions,
   and each file may have its own copying conditions.
:Maintainers:
   - Jashank Jeremy <jashank.jeremy@unsw.edu.au>

This is a library of code extracted from the FreeBSD project,
providing useful data structures and utilities.

Also included here are some scripts for extracting a file and its
complete history from the upstream FreeBSD source tree.

Most of this code is set up to support sel4httpd or pixie-c.


Versioning:
------------------------------------

The version number has the form *X.F.G*:

- *X*, the major, is the number of revisions to ``FILES``,
  the list of files extracted into this repository;
  bumping this implies a major ABI change.

- *F* is the FreeBSD -CURRENT major release number.

- *G* is the number of commits made to any of the extracted files.

So, for example, version 1.14.1827 indicates
revision 1 of ``FILES``,
using FreeBSD 14-CURRENT sources,
and where 1827 commits have occurred upstream
involving any of the files extracted.

This should produce an effect
not entirely dissimilar to,
*but not compatible with*,
Semantic Versioning:
in particular,
it is possible that changes land upstream
that a SemVer-compliant versioning scheme
must classify as a minor or major bump,
though here it will only appear as
a patch-level change.
