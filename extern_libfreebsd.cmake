#-
# Copyright 2021-22, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-2-Clause
#

cmake_minimum_required (VERSION 3.0.0)
project (libfreebsd
	VERSION		1.14.1833
	DESCRIPTION	"Support code exported from FreeBSD"
	HOMEPAGE_URL	"<https://freebsd.org/>"
	LANGUAGES	C
)

add_library (freebsd STATIC)
target_sources (freebsd PRIVATE
# style(9): kernel source file style guide
	${libfreebsd_root}/sys/sys/cdefs.h
	${libfreebsd_root}/sys/sys/param.h
	${libfreebsd_root}/sys/sys/types.h
	${libfreebsd_root}/sys/sys/_types.h
#	${libfreebsd_root}/share/man/man9/style.9

# queue(3): implementations of singly-linked lists, singly-linked tail
# queues, lists and tail queues
	${libfreebsd_root}/sys/sys/queue.h
#	${libfreebsd_root}/share/man/man3/queue.3

# tree(3): implementations of splay and rank-balanced (wavl) trees
	${libfreebsd_root}/sys/sys/tree.h
#	${libfreebsd_root}/share/man/man3/tree.3

# sbuf(9): safe string composition
	${libfreebsd_root}/sys/kern/subr_sbuf.c
	${libfreebsd_root}/sys/sys/sbuf.h
#	${libfreebsd_root}/share/man/man9/sbuf.9

# arb(3): array-based red-black trees
	${libfreebsd_root}/sys/sys/arb.h
#	${libfreebsd_root}/share/man/man3/arb.3

# bitset(9): bitset manipulation macros
	${libfreebsd_root}/sys/sys/bitset.h
	${libfreebsd_root}/sys/sys/_bitset.h
#	${libfreebsd_root}/share/man/man9/bitset.9

# bitstring(3): bit-string manipulation functions and macros
	${libfreebsd_root}/sys/sys/bitstring.h
#	${libfreebsd_root}/share/man/man3/bitstring.3

# byteorder(9): byte order operations
	${libfreebsd_root}/sys/sys/endian.h
	${libfreebsd_root}/sys/sys/_endian.h
#	${libfreebsd_root}/share/man/man9/byteorder.9
)

target_include_directories (freebsd PUBLIC ${libfreebsd_root})

set (freebsd_INCLUDE_DIRS	${libfreebsd_root})
set (freebsd_LIBRARIES		freebsd)
