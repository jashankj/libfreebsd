#-
# Copyright 2022, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-2-Clause
#

# Do an initial export.
#
# This assumes you have all the auxiliary files in `$work/libfreebsd',
# and will generate `$work/vendoring' (and a bunch of other stuff).

if [ -z "$sf" ] || [ -z "$lf" ] || [ -z "$work" ] || [ -z "$upstreamname" ]
then
	echo "INITIAL.sh: source \`CONFIG.sh' first!" >&2
fi

. "${admindir}/COMMON.sh"

# Set up new Git repository.
initial_1() {
	do_git_init "${work}/vendoring"
	cd "${work}/vendoring"

	do_update_auxfiles
	ln -s sys bsd
	git add bsd
	git commit -s -m "Initial commit (reroll 72)."
}

# Generate patchsets.
initial_2() {
	cd "${sf}"
	for f in $(ccat "${filesfile}")
	do
		echo -n "${f}"
		mkdir -p "${sf}/history/$(dirname "${f}")";
		do_patchlog "${f}" > "$(printf "${sf}/history/%s.patch" "${f}")"
		echo
	done
}

# Generate Git repositories
initial_3() {
	cd "${work}"
	for f in $(ccat "${filesfile}")
	do
		fx="${f:gs/\//__/}"
		d="${work}/history__${fx}"
		do_git_init "${d}"
		pushd "${d}"
		git am "${sf}/history/${f}.patch" || {
			echo ""
			echo "git am failed; emergency shell time."
			echo "exit when finished with series."
			zsh
		}
		popd
	done
}

initial_4() {
	do_octopus INITIAL $(ccat "${filesfile}")
	do_newvers "$(get_upstream_rev)"
}
