#-
# Copyright 2022, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-2-Clause
#

# Do an initial export.
#
# This assumes you have all the auxiliary files in `$work/libfreebsd',
# and will generate `$work/vendoring' (and a bunch of other stuff).

if [ -z "$sf" ] || [ -z "$lf" ] || [ -z "$work" ] || [ -z "$upstreamname" ]
then
	echo "COMMON.sh: source \`CONFIG.sh' first!" >&2
fi

get_upstream_rev() {
	git -C "${sf}" rev-parse HEAD
}

do_patchlog() {
	f="$1";
	oldrev="${2-HEAD}";

	git -P -C "${sf}" log \
		--reverse \
		--format=email \
		--patch-with-stat \
		--diff-merges=on \
		--binary \
		"${oldrev}" -- "${f}"
}

do_git_init() {
	d="$1"
	git init "${d}"
	git -C "${d}" config --local \
		user.email jashank.jeremy@unsw.edu.au
}


do_update_auxfiles() {
	for f in $(ccat "${auxfilesfile}")
	do
		mkdir -p "$(dirname "${f}")"
		cp "${lf}/${f}" "${f}"
		git add "${f}"
	done
}

# Octopus together the Git repositories.
# Regrettably, `-s octopus' can't cope with unrelated histories,
# so what you get is actually a long list of merges.
do_octopus() {
	cd "${work}/vendoring"

	modeh="$1"; shift;

	for f
	do
		fx="${f:gs/\//__/}"

		if [ "${modeh}" = INITIAL ]
		then
			d="${work}/history__${fx}"
			git fetch "${d}"
			git branch "history/${f}" FETCH_HEAD
		fi

		git merge \
			--no-commit \
			--allow-unrelated-histories \
			"history/${f}"

		"${admindir}/EXPORT1" "$upstreamname" "${sf}" "${f}"

		[ -e "${patchesdir}/${fx}" ] &&
			patch --strip=1 --verbose < "${patchesdir}/${fx}"

		git add "${f}"

		if [ "${modeh}" = INITIAL ]
		then
			git commit -s -m "Import ${f} (and history)."
		else
			git commit -s -m "Update ${f} from upstream (to ${modeh})."
		fi
	done
}

# Update version information.
do_newvers() {
	newhead="$1"

	echo "${newhead}" | tee "${upstreamfile}"
	git add "${upstreamfile}"

	newvers="$(${admindir}/VERSION)"
	echo "${newvers}" | tee "${versionfile}"
	git add "${versionfile}"

	# Update in specific files ---
	sed -i -Ee "s@:Version:.*\$@:Version: ${newvers}@" \
		README.rst
	git add README.rst

	sed -i -Ee "s@	VERSION.*\$@	VERSION		${newvers}@" \
		extern_libfreebsd.cmake
	git add extern_libfreebsd.cmake

	git commit --signoff -m "Version bump (now ${newvers})."
	git tag --sign --annotate "v${newvers}" \
		-m "Update to ${newvers} (from ${upstreamname}#${newhead})"
}
