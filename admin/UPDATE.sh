#-
# Copyright 2022, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-2-Clause
#

if [ -z "$sf" ] || [ -z "$lf" ] || [ -z "$work" ] || [ -z "$upstreamname" ]
then
	echo "UPDATE.sh: source \`CONFIG.sh' first!" >&2
fi

. "${admindir}/COMMON.sh"

# Fetch any upstream changes between the old and new heads.
update_1() {
	oldhead="$(cat "${upstreamfile}")"

	cd "${work}/vendoring"
	for f in $(ccat "${filesfile}")
	do
		git switch "history/${f}"
		do_patchlog "${f}" "${oldhead}..HEAD" -- "${f}" \
		| git am -
	done

	git switch trunk
}

# Merge forward any changes from upstream.
update_2() {
	newhead="$(get_upstream_rev)"
	do_octopus "${newhead}" "$@"
}

# Update version information.
update_3() {
	do_newvers "$(get_upstream_rev)"
}
