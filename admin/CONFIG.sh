#-
# Copyright 2022, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-2-Clause
#

# work: the directory to dump crap into
export work=~/ts/h

# lf: the "old" source tree.
export lf=$work/libfreebsd

# sf: a git-backed FreeBSD source tree, pointing at main/-CURRENT.
export sf=/src/freebsd/src-current

# filesfile: the list of files to vendor
export filesfile="${lf}/FILES"

# versionfile: the file indicating the current version
export versionfile="VERSION"

# upstreamfile: the file indicating the upstream revision
export upstreamfile="UPSTREAM-REV"

# upstreamname: the name of the upstream for VCS IDs
export upstreamname="FreeBSD/FreeBSD-src.git"

# admindir: the file directory containing all the management tools
export admindir="${lf}/admin"

# patchesdir: the file directory containing al lthe patches
export patchesdir="${lf}/patches"

# auxfilesfile: the list of files to vendor
export auxfilesfile="${admindir}/AUXFILES"


# Handy comment-stripping and whitespace-squeezing cat(1)-alike
ccat() {
	sed -E -e 's/#.*//g; /^[[:space:]]*$/d;' "$@"
}
