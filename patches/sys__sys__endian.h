From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
# Copyright 2002, Thomas Moestl <tmm@FreeBSD.org>
# Copyright 2021-22, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-2-Clause-Views

diff --git i/sys/sys/endian.h w/sys/sys/endian.h
index fca6fc9..3c0d0fb 100644
--- i/sys/sys/endian.h
+++ w/sys/sys/endian.h
@@ -34,7 +34,8 @@
 
 #include <bsd/sys/cdefs.h>
 #include <bsd/sys/_types.h>
-#include <machine/endian.h>
+//% #include <machine/endian.h>
+#include <bsd/sys/_endian.h>
 
 #ifndef _UINT8_T_DECLARED
 typedef	__uint8_t	uint8_t;
