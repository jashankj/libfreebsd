From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
# Copyright 2002 Mike Barcroft <mike@FreeBSD.org>
# Copyright 2021-22, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-2-Clause-Views

diff --git i/sys/sys/_types.h w/sys/sys/_types.h
index e866b1b8..a62a7163 100644
--- i/sys/sys/_types.h
+++ w/sys/sys/_types.h
@@ -111,44 +111,44 @@
 /*
  * Target-dependent type definitions.
  */
-#include <machine/_types.h>
+//% #include <machine/_types.h>
 
 /*
  * Standard type definitions.
  */
-typedef	__int32_t	__blksize_t;	/* file block size */
+//% typedef	__int32_t	__blksize_t;	/* file block size */
 typedef	__int64_t	__blkcnt_t;	/* file block count */
 typedef	__int32_t	__clockid_t;	/* clock_gettime()... */
 typedef	__uint32_t	__fflags_t;	/* file flags */
-typedef	__uint64_t	__fsblkcnt_t;
-typedef	__uint64_t	__fsfilcnt_t;
+//% typedef	__uint64_t	__fsblkcnt_t;
+//% typedef	__uint64_t	__fsfilcnt_t;
 typedef	__uint32_t	__gid_t;
-typedef	__int64_t	__id_t;		/* can hold a gid_t, pid_t, or uid_t */
-typedef	__uint64_t	__ino_t;	/* inode number */
-typedef	long		__key_t;	/* IPC key (for Sys V IPC) */
+//% typedef	__int64_t	__id_t;		/* can hold a gid_t, pid_t, or uid_t */
+//% typedef	__uint64_t	__ino_t;	/* inode number */
+//% typedef	long		__key_t;	/* IPC key (for Sys V IPC) */
 typedef	__int32_t	__lwpid_t;	/* Thread ID (a.k.a. LWP) */
-typedef	__uint16_t	__mode_t;	/* permissions */
+//% typedef	__uint16_t	__mode_t;	/* permissions */
 typedef	int		__accmode_t;	/* access permissions */
 typedef	int		__nl_item;
-typedef	__uint64_t	__nlink_t;	/* link count */
-typedef	__int64_t	__off_t;	/* file offset */
+//% typedef	__uint64_t	__nlink_t;	/* link count */
+//% typedef	__int64_t	__off_t;	/* file offset */
 typedef	__int64_t	__off64_t;	/* file offset (alias) */
 typedef	__int32_t	__pid_t;	/* process [group] */
 typedef	__int64_t	__sbintime_t;
-typedef	__int64_t	__rlim_t;	/* resource limit - intentionally */
+//% typedef	__int64_t	__rlim_t;	/* resource limit - intentionally */
 					/* signed, because of legacy code */
 					/* that uses -1 for RLIM_INFINITY */
 typedef	__uint8_t	__sa_family_t;
 typedef	__uint32_t	__socklen_t;
 typedef	long		__suseconds_t;	/* microseconds (signed) */
-typedef	struct __timer	*__timer_t;	/* timer_gettime()... */
+//% typedef	struct __timer	*__timer_t;	/* timer_gettime()... */
 typedef	struct __mq	*__mqd_t;	/* mq_open()... */
 typedef	__uint32_t	__uid_t;
 typedef	unsigned int	__useconds_t;	/* microseconds (unsigned) */
 typedef	int		__cpuwhich_t;	/* which parameter for cpuset. */
 typedef	int		__cpulevel_t;	/* level parameter for cpuset. */
 typedef int		__cpusetid_t;	/* cpuset identifier. */
-typedef __int64_t	__daddr_t;	/* bwrite(3), FIOBMAP2, etc */
+//% typedef __int64_t	__daddr_t;	/* bwrite(3), FIOBMAP2, etc */
 
 /*
  * Unusual type definitions.
@@ -201,7 +201,7 @@ typedef	__uint32_t	__fixpt_t;	/* fixed point number */
 typedef union {
 	char		__mbstate8[128];
 	__int64_t	_mbstateL;	/* for alignment */
-} __mbstate_t;
+} __bsd_mbstate_t;
 
 typedef __uintmax_t     __rman_res_t;
 
