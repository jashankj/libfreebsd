From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
# Copyright 1987, 1991, The Regents of the University of California.
# Copyright 2021-22, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-3-Clause

diff --git i/sys/sys/_endian.h w/sys/sys/_endian.h
index 9489935..41196a2 100644
--- i/sys/sys/_endian.h
+++ w/sys/sys/_endian.h
@@ -33,9 +33,9 @@
 #ifndef _SYS__ENDIAN_H_
 #define	_SYS__ENDIAN_H_
 
-#ifndef _MACHINE_ENDIAN_H_
-#error "sys/_endian.h should not be included directly"
-#endif
+//% #ifndef _MACHINE_ENDIAN_H_
+//% #error "sys/_endian.h should not be included directly"
+//% #endif
 
 /* BSD Compatiblity */
 #define	_BYTE_ORDER	__BYTE_ORDER__
